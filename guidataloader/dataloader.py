import PySimpleGUI as sg
import importfunctions as imf
import pandas as pd 

sg.theme('Dark Teal')   # Add a touch of color

# All the stuff inside your window.
layout = [[sg.T("Data Import")], 
          [sg.T("CH4"), sg.InputText(default_text='0', k='CH4MFIN', size=(3, 1)),sg.InputText(default_text='0', k='CH4STDEVIN', size=(3, 1))],
          [sg.T("CO2"), sg.InputText(default_text='0', k='CH4MFIN', size=(3, 1)),sg.InputText(default_text='0', k='CH4STDEVIN', size=(3, 1))],
          [sg.T("N2"), sg.InputText(default_text='0', k='CH4MFIN', size=(3, 1)),sg.InputText(default_text='0', k='CH4STDEVIN', size=(3, 1))],
          [sg.T("He"), sg.InputText(default_text='0', k='CH4MFIN', size=(3, 1)),sg.InputText(default_text='0', k='CH4STDEVIN', size=(3, 1))],
          [sg.Text('IN', size=(8, 1)), sg.Input(),sg.FileBrowse(initial_folder=".",k='BrowseIN')],
          [sg.Text('OUT', size=(8, 1)), sg.Input(),sg.FileBrowse(initial_folder=".",k='BrowseOUT')],
          [sg.Button('Loadcsv'),sg.Button("Save Data",disabled=True),sg.Button('Confirm')]]

# Create the Window
window = sg.Window('Lattice Spring Generator', layout)
dfIN = pd.DataFrame
dfOUT = pd.DataFrame

# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED:  # if user closes window or clicks cancel
      break
    if event == "Loadcsv":
      dfIN = imf.csvimport(values['BrowseIN'],'In')
      dfOUT = imf.csvimport(values['BrowseOUT'],'Out')
      window["Save Data"].Update(disabled=False)
    if event == "Save Data":
      experimentname = "experiment"
      dfIN.to_csv(experimentname+"IN.csv",index = False)
      dfOUT.to_csv(experimentname+"OUT.csv",)
    #if event == 'Confirm':
    #  df = assembledata(values)
    #  exportcsv(df)

window.close()