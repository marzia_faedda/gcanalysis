import pandas as pd
from pandas.core.frame import DataFrame
from pandas.core.indexes.base import Index 

mfnames = ["MolFrac","StDev"]

def generatecolumnnames(colnames, inorout):
  return [colnames[0]+inorout,colnames[1]+inorout]

def csvimport(csvname, inorout):
  df = pd.read_csv(csvname, sep=",", index_col=0, names=generatecolumnnames(mfnames,inorout)) 
  print(df)
  return df

def createemptydfIN(inorout):
  df = pd.DataFrame
  df(index=["CH4","CO2","N2","He"],columns=generatecolumnnames(mfnames,inorout))
  return df





#Import from csv
#TODO(@marzia.faedda@unitn.it) check separator
#dfIN  = pd.read_csv('input_example_IN.txt', sep=",", index_col=0, names=['MolFracIN','StDevIN'])
#dfOUT = pd.read_csv('input_example_OUT.txt', sep=",", index_col=0, names=['MolFracOUT','StDevOUT'])




#DEBUG ROWS
#print(dfIN)
#print(dfOUT)

